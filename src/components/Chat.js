import React from "react";
import Header from './Header/Header';
import MessageList from './MessageList/MessageList';
import MessageInput from './MessageInput/MessageInput';
import LoadingPage from './LoadingPage';
import './stylesChat/reset.css'
import './stylesChat/styles.css';

class Chat extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = { messages: [] };
    this.updateState = this.updateState.bind(this);
  }

  updateState(state) {
    this.setState({ state });
  }

  componentDidMount() {
    fetch("https://api.myjson.com/bins/1hiqin", {
      method: "GET"
    }).then(response => response.json())
      .then(messages => this.setState({ messages }))
      .catch(error => new Error(error));
  }

  render() {
    if(this.state.messages.length) {
      return (
        <div className="Chat">
          <Header messages={this.state.messages} />
          <MessageList messages={this.state.messages} parentState={this.updateState} />
          <MessageInput messages={this.state.messages} parentState={this.updateState} />
        </div>
      );
    } else {
      return <LoadingPage />
    }
  }
}

export default Chat;