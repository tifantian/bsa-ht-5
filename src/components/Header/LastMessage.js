import React from 'react';
import getDateString from '../helpers/date'

class LastMessage extends React.Component {

  lastMessageTime() {
    const lengthMessagesArray = this.props.messages.length;
    const oneMessage = this.props.messages[lengthMessagesArray - 1];
    const date = (oneMessage) ? oneMessage.created_at : null;
    return getDateString(date);
  }

  render() {
    return (
      <div className='header-last-message'>
        Last message {this.lastMessageTime()}
      </div>
    )
  }
}

export default LastMessage;