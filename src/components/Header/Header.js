import React from "react";
import TotalUsers from './TotalUsers';
import TotalMessages from './TotalMessages';
import LastMessage from './LastMessage';

class Header extends React.Component {
  
  render() {
    return (
      <header className='page-header'>
        <div className='header-chat-name'>
          myChat
        </div>
        <TotalUsers messages={this.props.messages}/>
        <TotalMessages messages={this.props.messages}/>
        <LastMessage messages={this.props.messages}/>
      </header>
    )
  }
}

export default Header;