import React from 'react';

class TotalUsers extends React.Component {

  getAllUsersCount() {
    const counts = {};
    this.props.messages.forEach(element => counts[element.user] = null);
    return Object.keys(counts).length;
  }

  render() {
    return (
      <div className='header-total-users'>
        {this.getAllUsersCount()} users
      </div>
    )
  }
}

export default TotalUsers;