import React from 'react';

class TotalMessages extends React.Component {

  getAllMessagesCount() {
    return this.props.messages.length;
  }

  render() {
    return (
      <div className='header-total-messages'>
        {this.getAllMessagesCount()} messages
      </div>
    )
  }
}

export default TotalMessages;