import React from 'react'; 
import  getDateString  from '../helpers/date'

const userMe = 'Koteiko'; // You should set 'Dave'

// Мне надо юзнуть parentState чтобы вписать туда новую инфу про лайки, прочитку сообщений
// По этому же стейту стоит удалять сообщения

class Message extends React.Component { // have oneMessage
  
  getMessage() { // достать аватарку, дату, сообщение
    const objectMessage = this.props.oneMessage;
    return (objectMessage) ? objectMessage.message : null;
  }

  getLogoSrc() {
    const objectMessage = this.props.oneMessage;
    return (objectMessage) ? objectMessage.avatar : null;
  }

  getDate() {
    const objectMessage = this.props.oneMessage;
    const date = (objectMessage) ? objectMessage.created_at : null;
    return getDateString(date);

  }

  getAltPropForImg() {
    const objectMessage = this.props.oneMessage;
    return (objectMessage) ? `photo-of-${objectMessage.user}` : null;
  }

  getAuthor() {
    const objectMessage = this.props.oneMessage;
    return (objectMessage) ? objectMessage.user : null;
  }
  
  render() {
    return (
      <div className={this.getAuthor() === userMe ? "main-message my-message" : "main-message"}>
        <div className="message-logo">
          <img src={this.getLogoSrc()} alt={this.getAltPropForImg()} />
        </div>
        <div className="message-info">
          <div className="message-author">
            {this.getAuthor()}
          </div>
          <div className="message-text">
            {this.getMessage()}
          </div>  
          <div className="message-date">
            {this.getDate()}
          </div>
        </div>      
      </div>
    )
  }
}

export default Message;