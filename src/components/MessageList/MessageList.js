import React from 'react'; 
import Message from './Message'

class MessageList extends React.Component {

  getAllMessages() {
    let keyForMessage = 0;
    const allUsersArray = this.props.messages.map( (oneMess) => {
      return <Message oneMessage={oneMess} key={keyForMessage++} parentState={this.props.parentState}/>;
    });
    return allUsersArray;
  }

  render() {
    return (
      <main className="message-list"> 
        {this.getAllMessages()}
      </main>
    )
  }
}

export default MessageList;