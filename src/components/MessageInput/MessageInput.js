import React from 'react';

class MessageInput extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      bodyMessage: ''
    };
  }

  handleAddPost() {
    const { messages, parentState } = this.props;
    const { bodyMessage } = this.state;
    const d = new Date();
    const dateNow = d.getFullYear() + "-" +
      ("00" + (d.getMonth() + 1)).slice(-2) + "-" +
      ("00" + d.getDate()).slice(-2) + " " +
      ("00" + d.getHours()).slice(-2) + ":" +
      ("00" + d.getMinutes()).slice(-2) + ":" +
      ("00" + d.getSeconds()).slice(-2);
    if (bodyMessage.trim().length > 0) {
      const newPost = {
        id: '0668300638',
        user: 'Koteiko',
        avatar: "https://images.pexels.com/photos/617278/pexels-photo-617278.jpeg?",
        created_at: dateNow,
        message: this.state.bodyMessage,
        marked_read: false
      }
      messages.push(newPost);
      parentState(messages);
      this.setState({ bodyMessage: '' });
    }
  }

  render() {
    return (
      <div className='message-input-block'>
        <input type="text" className="message-input" onChange={e => this.setState({ bodyMessage: e.target.value })} value={this.state.bodyMessage}/>
        <button type="submit" className="message-submit" onClick={e => this.handleAddPost()}>Submit</button>
      </div>
    )
  }
}

export default MessageInput;