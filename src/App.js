import React from "react";
import Chat from './components/Chat';

class App extends React.Component {
  
  render() {
    return (
      <Chat />
    );
  }

}

export default App;